import java.util.ArrayList;

public class Main {
	
	public static void main(String[] args) throws Exception{
		
		// Pour produire plusieurs courbes %success/durete
		
//		benchMarkForDensities();
		
		// Pour produire une seul courbe %success/durete
		benchMarkForDensity(40, 10, 5);
		
		// Pour avoir la dureté de la transition
//		for (int densite=10;densite<90;densite+=10)
//			System.out.println(densite + ": " +getTransition(densite));
//		
		System.out.println("Fini");
		return;
	}
	
	private static void benchMarkForDensities() throws Exception {
		int nbPoint = 10;
		int nbRes = 10;
		
		for(int densite=10; densite<=90;densite+=10)
			benchMarkForDensity(densite, nbRes, nbPoint);
		
		return;
	}
	
	private static void benchMarkForDensity(int densite, int nbRes, int nbPoint) throws Exception {
		// Initialisation des variables
		int dureteMin = 25; // 5
		int dureteMax = 45; // 95
		
		int nbVariable = 35; // 20
		int nbConstraints = densite*nbVariable*(nbVariable-1)/200;

		int tailleDomaine = 17; // 12
		int nbTuplesMin = (100-dureteMax)*tailleDomaine*tailleDomaine/100;
		int nbTuplesMax = (100-dureteMin)*tailleDomaine*tailleDomaine/100;

		int dT = (nbTuplesMax-nbTuplesMin)/(nbPoint-1);

		
		ArrayList<Double> SUCCES = new ArrayList<Double>();
		System.out.println("Densité " + densite +"; n="+nbVariable+"; c="+nbConstraints+"; d="+tailleDomaine);
		// PREMIER BALAYAGE
		System.out.println("PREMIER BALAYAGE");
		
		String fileName = "results/den" + densite +".ods";
		Runtime.getRuntime().exec("rm "+fileName);
		for(int nbTuples=nbTuplesMin; nbTuples<=nbTuplesMax ; nbTuples+=dT) {
			Expe benchmark = new Expe(nbVariable, tailleDomaine, nbConstraints, nbTuples, nbRes);
			SUCCES.add(benchmark.run(fileName));
			
			float pourcentage = ((float) (nbTuples - nbTuplesMin))/(nbTuplesMax - nbTuplesMin)* 100;
			System.out.println(nbTuples + " tuples - " + Math.round(pourcentage) + "%");
		}
		
		int eps = 5;
		int tempMin = nbTuplesMin;
		
		for(int i=1; i<nbPoint-1;i++)
			if(SUCCES.get(i)>eps) {
				tempMin = nbTuplesMin + (i-1)*dT;
				break;
			}
		for(int i=nbPoint-2; i>=0;i--)
			if(SUCCES.get(i)<100-eps) {
				nbTuplesMax =  nbTuplesMin + (i+1)*dT;
				break;
			}
		
		nbTuplesMin = tempMin;
		
		dT = (nbTuplesMax-nbTuplesMin)>(nbPoint+1) ? (nbTuplesMax-nbTuplesMin)/(nbPoint+1) : 1;
		
		// SECOND BALAYGE
		System.out.println("SECOND BALAYAGE");
		for(int nbTuples=nbTuplesMin+dT; nbTuples<=nbTuplesMax-dT ; nbTuples+=dT) {
			Expe benchmark = new Expe(nbVariable, tailleDomaine, nbConstraints, nbTuples, nbRes);
			benchmark.run(fileName);
			
			float pourcentage = ((float) (nbTuples - nbTuplesMin))/(nbTuplesMax - nbTuplesMin)* 100;
			System.out.println(nbTuples + " tuples - " + Math.round(pourcentage) + "%");
		}
	}
	
	
	private static double getTransition(int densite) throws Exception {
		// Initialisation des variables
		int nbVariable = 20;
		int nbConstraints = densite*nbVariable*(nbVariable-1)/200;
		
		int durete = 10;
		int tailleDomaine = 10;
		int nbTuples = (100-durete)*tailleDomaine*tailleDomaine/100;
				
		int Ddurete = 10;
		int nbRes = 30;
		
		// On avance jusqu'à la transtition
		Expe benchmark = new Expe(nbVariable, tailleDomaine, nbConstraints, nbTuples, nbRes);
		double trans = benchmark.run();
		while(trans>50) {
			durete += Ddurete;
			nbTuples = (100-durete)*tailleDomaine*tailleDomaine/100;
			benchmark = new Expe(nbVariable, tailleDomaine, nbConstraints, nbTuples, nbRes);
			trans = benchmark.run();
		}
		
		durete -= 1;
		nbTuples = (100-durete)*tailleDomaine*tailleDomaine/100;
		benchmark = new Expe(nbVariable, tailleDomaine, nbConstraints, nbTuples, nbRes);

		double tempTrans = trans;
		trans = benchmark.run();
		
		// On recule 
		while(trans<50) {
			tempTrans = trans;
			durete -= 1;
			nbTuples = (100-durete)*tailleDomaine*tailleDomaine/100;
			benchmark = new Expe(nbVariable, tailleDomaine, nbConstraints, nbTuples, nbRes);
			trans = benchmark.run();
		}
		
		// Approximation linéaire
		return (durete + (50-trans)/(tempTrans-trans));
	}
}
