import org.chocosolver.solver.Model;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.constraints.extension.Tuples;


public class Nreines {
	
	private static int n = 1;

    public Nreines (int n) {
        Nreines.n = n;
    }

	public static void main(String[] args) {
		
		// Création du modele
		Model model = new Model("Reines");
		
		// Création des variables (tableau de n variables entières de domaine [1, n]
		// Pour chaque variable, sa ligne est sa position dans le tableau
		IntVar [] tab_reines = model.intVarArray("x", n, 1, n);


		// Création des contraintes
		// Interdit d'être sur la même ligne
	    // -> Naturel de par la modélisation qu'on a choisie
		
		// Interdit d'être sur la même colonne
	    model.allDifferent(tab_reines).post();
		
		// Interdit d'être sur la même diagonale
		// Contrainte arithmétique :
		// 		colonne Ri != colonne Rj + (j-i)
		// 		colonne Ri != colonne Rj - (j-i)
	    // allDifferent(X1+1, X2+2, … Xn+n)
	    // allDifferent(X1-1, X2-2, … Xn-n)
		for (int i = 0; i < n-1; i++) {
			for (int j = 1; i+j < n; j++) {
				model.arithm(tab_reines[i+j], "!=", tab_reines[i], "+", j).post();
				model.arithm(tab_reines[i+j], "!=", tab_reines[i], "-", j).post();
			}
		}
		
		
	
        // Affichage du réseau de contraintes créé
        System.out.println("*** Réseau Initial ***");
        System.out.println(model);
        

        // Calcul de la première solution
        if(model.getSolver().solve()) {
        	System.out.println("\n\n*** Première solution ***");
        	for (int i = 0; i < n; i++) {
        		System.out.println("" + i + tab_reines[i]);
    		}
        	
        }
   
    	// Calcul de toutes les solutions
    	System.out.println("\n\n*** Autres solutions ***");        
        while(model.getSolver().solve()) {    	
            System.out.println("Sol "+ model.getSolver().getSolutionCount()+"\n"+model);
	    }   
        
        // Affichage de l'ensemble des caractéristiques de résolution
      	System.out.println("\n\n*** Bilan ***");        
        model.getSolver().printStatistics();
        
        
        
        // Affichage du bilan de l'exercice
        System.out.println("\n\n\n\n\n\n\n*** BILAN DE L'EXERCICE ***");
        System.out.println("1 reines -> 1           solution");
        System.out.println("2 reines -> 0           solution");  
        System.out.println("3 reines -> 0           solution");  
        System.out.println("4 reines -> 2           solutions");  
        System.out.println("8 reines -> 92          solutions");  
        System.out.println("12 reines -> 14 200     solutions");
        System.out.println("16 reines -> 14 772 512 solutions");  
        System.out.println("\nDonc pour tout n >= 4, le nombre de solutions augmente très rapidement."); 
	}
}
