import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Locale;

import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.extension.Tuples;
import org.chocosolver.solver.search.strategy.Search;
import org.chocosolver.solver.search.strategy.strategy.AbstractStrategy;
import org.chocosolver.solver.variables.IntVar;

import com.opencsv.CSVWriter;

public class Expe {
	
	private int nbVariable, tailleDomaine, nbConstraints, nbTuples, nbRes;
	
	private float densite() {return (float) (200.*nbConstraints/(nbVariable*(nbVariable-1)));}
	private float durete() {return (float) (100.*(tailleDomaine*tailleDomaine-nbTuples)/(tailleDomaine*tailleDomaine));}
	
	public Expe(int nbVariable, int tailleDomaine, int nbConstraints, int nbTuples, int nbRes) {
		this.nbVariable = nbVariable;
		this.tailleDomaine = tailleDomaine;
		this.nbConstraints = nbConstraints;
		this.nbTuples = nbTuples;
		this.nbRes = nbRes;
	}
	
	private void creerReseau() throws IOException {
		// Execute la commande : 
		// ./benchmarks/urbcsp nbVariable tailleDomaine nbConstraints nbTuples nbRes
		String command = "./benchmarks/urbcsp";
		command += " " + nbVariable;
		command += " " + tailleDomaine;
		command += " " + nbConstraints;
		command += " " + nbTuples;
		command += " " + nbRes;
		Process process = Runtime.getRuntime().exec(command);
	
		// Ecrit le résulat dans un fichier txt
		printResults(process);
		
		return;
	}
	
	private static void printResults(Process process) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
	    String line = "";
	    String content = "";
	    while ((line = reader.readLine()) != null) {
	        content += line + "\n";
	    }

		 File file = new File("./benchmarks/tmp.txt");

		 // Créer le fichier s'il n'existe pas
		 if (!file.exists()) {
		 file.createNewFile();
		 }

		 FileWriter fw = new FileWriter(file.getAbsoluteFile());
		 BufferedWriter bw = new BufferedWriter(fw);
		 bw.write(content);
		 bw.close();
	}
	
	private static Model lireReseau(BufferedReader in) throws Exception{
			Model model = new Model("Expe");
			int nbVariables = Integer.parseInt(in.readLine());				// le nombre de variables
			int tailleDom = Integer.parseInt(in.readLine());				// la valeur max des domaines
			IntVar []var = model.intVarArray("x",nbVariables,0,tailleDom-1); 	
			int nbConstraints = Integer.parseInt(in.readLine());			// le nombre de contraintes binaires		
			for(int k=1;k<=nbConstraints;k++) { 
				String chaine[] = in.readLine().split(";");
				IntVar portee[] = new IntVar[]{var[Integer.parseInt(chaine[0])],var[Integer.parseInt(chaine[1])]}; 
				int nbTuples = Integer.parseInt(in.readLine());				// le nombre de tuples		
				Tuples tuples = new Tuples(new int[][]{},true);
				for(int nb=1;nb<=nbTuples;nb++) { 
					chaine = in.readLine().split(";");
					int t[] = new int[]{Integer.parseInt(chaine[0]), Integer.parseInt(chaine[1])};
					tuples.add(t);
				}
				model.table(portee,tuples).post();	
			}
			in.readLine();
			return model;
	}	
	
	/** Méthode qui calcule le pourcentage de success, le temps moyen de résolution et le nombre moyen de noeud créée
	 *  Range ces donnés dans un tableaur
	 * 
	 * @param csvFileName
	 * @return pourcentage de success
	 * @throws Exception
	 */
	public double run(String csvFileName) throws Exception{
		// Création du réseau
		creerReseau();
		
		ArrayList<ArrayList<Float>> temps = new ArrayList<ArrayList<Float>>();
		ArrayList<ArrayList<Long>> nbNode = new ArrayList<ArrayList<Long>>();
		
		for(int k = 0;k<nbRes;k++) {
			temps.add(new ArrayList<Float>());
			nbNode.add(new ArrayList<Long>());
		}
		
		// Lecture du réseau
		int succes = 0;
		int TO = 0;
		
		// On test 5 fois chaque réseau et on enlève les extrèmes
		for(int k=0;k<5;k++) {
			BufferedReader readFile = new BufferedReader(new FileReader("./benchmarks/tmp.txt"));
		
			Solver solver = null;
			
			// On test tout les réseaux
			for(int nb=0 ; nb<nbRes; nb++) {
				// On récupère le solver
				Model model = lireReseau(readFile);
				solver = model.getSolver();	
				
				if(solver==null) {
					System.out.println("Problème de lecture de fichier !\n");
					return 0.;
				}

				// On pose une limite de temps
				solver.limitTime("30s");
				
				// On choisi l'heuristique
				AbstractStrategy<?> strategie = Search.defaultSearch(model);
//				AbstractStrategy<?> strategie = Search.activityBasedSearch(model.retrieveIntVars(false));
//				AbstractStrategy<?> strategie = Search.minDomLBSearch(model.retrieveIntVars(false));
				solver.setSearch(strategie);
				
				if(solver.solve()){
					// Si on a résolue le réseau
					succes++;
					temps.get(nb).add(solver.getTimeCount());
					nbNode.get(nb).add(solver.getNodeCount());
				}else if(solver.isStopCriterionMet()){;
					// Si le programme a mis trop de temps
					TO++;
				}
				else {
					temps.get(nb).add(solver.getTimeCount());
					nbNode.get(nb).add(solver.getNodeCount());
				}
			}
			
		}

		double tempsMoy = 0;
		double nodeMoy = 0;
		
		for(int k=0;k<nbRes;k++) {
			temps.get(k).sort(null);
			nbNode.get(k).sort(null);
			for(int i=1;i<temps.get(k).size();i++) {
				tempsMoy += temps.get(k).get(i);
				nodeMoy += nbNode.get(k).get(i);
			}
		}
		
		tempsMoy /= (3*nbRes-TO);
		nodeMoy /= (3*nbRes-TO);
		
		double pcSucces = (double) ((TO!=5*nbRes)?succes*100./(5*nbRes-TO):0);
		double pcTO = (double)100.*TO/ (5*nbRes);
		
//		System.out.println("Densité : " + densite() +"% ; durete : " + durete() + "%");
//		System.out.println("% de succès : " + pcSucces + "%");
//		System.out.println("% de time-out : " + pcTO + "%");
//		System.out.println("temps moyen : " + tempsMoy + "s");
		
		
		// ÉCRITURE DANS UN FICHIER CSV
		File file_data = new File("./" + csvFileName);
		DecimalFormat formatter = new DecimalFormat("#.######", new DecimalFormatSymbols(Locale.FRENCH));
		
		// Si le fichier n'a pas encore été créé
		if (!file_data.exists()) {
			// create FileWriter object with file as parameter
			file_data.createNewFile();
				  
			FileWriter outputfile = new FileWriter(file_data);
			CSVWriter writer = new CSVWriter(outputfile);
			
	        // adding header to csv
	        String[] header = {"Densité", "Dureté", "Pourcentage de succès", "Temps moyen en secondes", "Nombre moyen de node créée", "Pourcentage de TO"};// JE NE SAIS PAS ECRIRE créée JE CROIS DESO
	        writer.writeNext(header);
	        
	        // add data to csv
	        String[] data_line = {formatter.format(densite()), formatter.format(durete()), formatter.format(pcSucces), formatter.format(tempsMoy),formatter.format(nodeMoy), formatter.format(pcTO)};
	        writer.writeNext(data_line);
	        
	        writer.close();
		}
		else {
			FileWriter outputfile = new FileWriter(csvFileName,true);
			CSVWriter writer = new CSVWriter(outputfile);
			
			// add data to csv
	        String[] data_line = {formatter.format(densite()), formatter.format(durete()), formatter.format(pcSucces), formatter.format(tempsMoy),formatter.format(nodeMoy), formatter.format(pcTO)};
	        writer.writeNext(data_line);
	        
	        writer.close();
		}
		
		return pcSucces;	
	}
	
	/** Méthode qui calcule simplement le pourcentage de success du réseau
	 * 
	 * @return pourcentage de success
	 * @throws Exception
	 */
	public double run() throws Exception{
		// Création du réseau
		creerReseau();
		
		ArrayList<ArrayList<Float>> temps = new ArrayList<ArrayList<Float>>();
		ArrayList<ArrayList<Long>> nbNode = new ArrayList<ArrayList<Long>>();
		
		for(int k = 0;k<nbRes;k++) {
			temps.add(new ArrayList<Float>());
			nbNode.add(new ArrayList<Long>());
		}
		
		// Lecture du réseau
		int succes = 0;
		int TO = 0;
		
		// On test 5 fois chaque réseau et on enlève les extrèmes
		for(int k=0;k<5;k++) {
			BufferedReader readFile = new BufferedReader(new FileReader("./benchmarks/tmp.txt"));
		
			Solver solver = null;
			
			// On test tout les réseaux
			for(int nb=0 ; nb<nbRes; nb++) {
				// On récupère le solver
				Model model = lireReseau(readFile);
				solver = model.getSolver();	
				
				if(solver==null) {
					System.out.println("Problème de lecture de fichier !\n");
					return 0.;
				}else if(solver.isStopCriterionMet()){;
					// Si le programme a mis trop de temps
					TO++;
				}

				// On pose une limite de temps
				solver.limitTime("30s");
				
				// On choisi l'heuristique
				AbstractStrategy<?> strategie = Search.defaultSearch(model);
//				AbstractStrategy<?> strategie = Search.activityBasedSearch(model.retrieveIntVars(false));
//				AbstractStrategy<?> strategie = Search.minDomLBSearch(model.retrieveIntVars(false));
				solver.setSearch(strategie);
				
				if(solver.solve()){
					// Si on a résolue le réseau
					succes++;
				}
			}
			
		}

		double pcSucces = (double) ((TO!=5*nbRes)?succes*100./(5*nbRes-TO):0);
				
		return pcSucces;
	}
	
}