# Compte-Rendu partiel du projet

Les paramètres:

* c : #Contraintes
* n : #Variables
* k : #Arité (k=2)
* t : #Tuples par contrainte
* d : #Domaine des variable


Densité :
$$
D_{t,d}(c,n) = \frac{2c}{n(n+1)}
$$

Dureté :
$$
T_{c,n}(t,d) = \frac{d^2-t}{d^2}
$$